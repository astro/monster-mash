x, y = 400, 300
s2 = 0
sy = 0
pp = 0
gegner_intervall = 0
gegner = {}
gegner_geboren = 0
raketen = {}
explosionen = {}
freund = { x = 200, y = 300 }
rakete_laden = 0

function love.load()
  robot = love.graphics.newImage("robot.png")
  flame = love.graphics.newImage("flame.png")
  parachute = love.graphics.newImage("parachute.png")
  feuerwerksrakete = love.graphics.newImage("feuerwerksrakete.png")
  faust = love.graphics.newImage("faust.png")
  kingkong = love.graphics.newImage("kingkong.png")
  geist = love.graphics.newImage("geist.png")
  lindwurm = love.graphics.newImage("lindwurm.png")
  baummonster = love.graphics.newImage("baummonster.png")
  steinmonster = love.graphics.newImage("steinmonster.png")

  -- font = love.graphics.newFont("Grundschrift-Normal.otf", 24)
  -- love.graphics.setFont(font)
end

function love.update(dt)
  -- Bewegung eigener Roboter
  if love.keyboard.isDown("left") then
    x = x - 100 * dt
  end
  if love.keyboard.isDown("right") then
    x = x + 100 * dt
  end
  if x < 0 then
    x = 0
  end
  if x > 800 - 32 then
    x = 800 - 32
  end
  
  if love.keyboard.isDown("up") then
    sy = sy - 1 * dt
  end
  if love.keyboard.isDown("down") then
    sy = sy + 1 * dt
  end
  if sy > 1 then
    sy = 1
  end
  y = y + sy
  if y > 600 - 32 then
    y = 600 - 32
    sy = 0
  end
  if y < 0 and sy < 0 then
    sy = -sy
  end

  -- Rakete losschiessen
  rakete_laden = rakete_laden + dt
  if love.keyboard.isDown("space") and rakete_laden > 0.1 then
    rakete_laden = 0
    table.insert(raketen, { x = x, y = y, s = 10 })
  end

  -- Raketen weiterfliegen lassen
  for r, rakete in ipairs(raketen) do
    rakete.s = rakete.s * 1.1
    rakete.y = rakete.y - rakete.s * dt
  end
  -- Gegner treffen mit Rakete
  for g, gegner1 in ipairs(gegner) do
    for r, rakete in ipairs(raketen) do
      if rakete.x > gegner1.x - 16 and rakete.x < gegner1.x + 16 and rakete.y > gegner1.y - 16 and rakete.y < gegner1.y + 16 then
        gegner1.leben = gegner1.leben - 25
        table.remove(raketen, r)
        print("neue explosion")
        table.insert(explosionen, { x = rakete.x, y = rakete.y, t = 0 })
      end
    end
  end
  for g, gegner1 in ipairs(gegner) do
    if gegner1.leben < 1 then
      table.remove(gegner, g)
    end
  end
  for r, rakete in ipairs(raketen) do
    -- Oberer Bildschirmrand
    if rakete.y < 0 then
      table.remove(raketen, r)
    end
  end
  for i, explosion in ipairs(explosionen) do
    explosion.t = explosion.t + dt
    if explosion.t > 1 then
      table.remove(explosionen, i)
    end
  end

  -- Neue Gegner
  gegner_intervall = gegner_intervall + dt
  if gegner_intervall > 0 then
    gegner_intervall = -3 
    local bild = math.floor(4 * math.random())
    local kraft = 0.4 * math.random()
    if bild == 0 then
      kraft = 10 * kraft
    end
    if gegner_geboren % 30 == 29 then
      table.insert(gegner, { x = 100 + 600 * math.random(), y = -100, s = 0, kraft = kraft, leben = 400, bild = 4 })
    else
      table.insert(gegner, { x = 100 + 600 * math.random(), y = -100, s = 0, kraft = kraft, leben = 100, bild = bild })
    end
    gegner_geboren = gegner_geboren + 1
  end
  -- Bewegung Gegner
  for i, g in ipairs(gegner) do
    if g.x > x then
      g.s = g.s - g.kraft * dt
    end
    if g.x < x then
      g.s = g.s + g.kraft * dt
    end
    if g.s > 100 * dt then
      g.s = 100 * dt
    end
    if g.s < -100 * dt then
      g.s = -100 * dt
    end
    g.x = g.x + g.s
    if g.x < 0 then
      g.x = 0
    end
    if g.x > 800 - 32 then
      g.x = 800 - 32
    end
    if g.y > y then
      g.y = g.y - 100 * dt
    end
    if g.y < y then
      g.y = g.y + 100 * dt
    end
  end

  -- Bewegung Freund
  if table.getn(gegner) > 0 then
    local g = gegner[1]
    if freund.x < g.x then
      freund.x = freund.x + 100 * dt
    end
    if freund.x > g.x then
      freund.x = freund.x - 100 * dt
    end
    if freund.y < g.y then
      freund.y = freund.y + 100 * dt
    end
    if freund.y > g.y and g.y > 0 then
      freund.y = freund.y - 100 * dt
    end
  end
  -- Freund verpruegelt Gegner
  for g, gegner1 in ipairs(gegner) do
    if gegner1.x > freund.x and gegner1.x < freund.x + 32 and gegner1.y > freund.y and gegner1.y < freund.y + 32 then
      gegner1.leben = gegner1.leben - 30 * dt
    end
  end
end

function love.draw()
  love.graphics.setColor(1,1,1, 1)
  love.graphics.setBackgroundColor(0.5, 0.5, 1)
  
  if sy < 0 then
    love.graphics.draw(flame, x, y + 32)
  end
  if sy > 0 and pp < 29 then
    pp = pp + 1
  end
  if sy <= 0 and pp > 0 then
    pp = pp - 1
  end
  if pp > 0 then
    love.graphics.draw(parachute, x, y - pp)
  end
  love.graphics.draw(robot, x, y)

  -- Durch alle Gegner
  for i, g in ipairs(gegner) do
    -- Zeichne
    if g.bild == 0 then
      love.graphics.draw(lindwurm, g.x, g.y)
    elseif g.bild == 1 then
      love.graphics.draw(geist, g.x, g.y)
    elseif g.bild == 2 then
      love.graphics.draw(robot, g.x, g.y)
    elseif g.bild == 3 then
      love.graphics.draw(baummonster, g.x, g.y)
    elseif g.bild == 4 then
      love.graphics.draw(steinmonster, g.x, g.y)
    end
    -- Setze Balkenfarbe
    love.graphics.setColor(1 - (g.leben / 100), g.leben / 100, 0, 1)
    -- Zeichne Balken fuer leben
    love.graphics.rectangle("fill", g.x, g.y + 32, 32 * g.leben / 100, 4, 0, 0, 0)

    love.graphics.setColor(1,1,1,1)
    -- Zeichne Faust
    if g.x >= x - 16 and g.x < x + 16 then
      love.graphics.draw(faust, g.x + 16, g.y)
    end
  end

  love.graphics.draw(kingkong, freund.x, freund.y)

  for i, rakete in ipairs(raketen) do
    love.graphics.draw(feuerwerksrakete, rakete.x, rakete.y)
  end
  for i, explosion in ipairs(explosionen) do
    love.graphics.setColor(1, 1 - explosion.t, 0, 1 - explosion.t)
    love.graphics.circle("fill", explosion.x, explosion.y, 100 * explosion.t, 10)
  end

  love.graphics.setColor(1,1,1, 1)
  -- love.graphics.print(math.floor(x).."             "..math.floor(y).." "..sy, 0, 0)
  love.graphics.print("Gegner: "..(#gegner), 0, 0)
end
